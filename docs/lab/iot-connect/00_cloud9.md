## Lab0:实验环境准备

在东京（ap-northeast-1）区域启动Cloud9 Web IDE，用于后续模拟IoT设备。
请按照下面的步骤创建用于实验的IDE环境。

1. 菜单栏搜索“Cloud9”，点击搜索结果
![](./md_image/cloud9/cloud9_1.png)

1. 点击“Create enviroment”按钮，开启创建过程
![](./md_image/cloud9/cloud9_2.png)

1. 录入Cloud9名称，如“IoTDay”
![](./md_image/cloud9/cloud9_3.png)

1. 如截图所示配置环境类型、实例类别及操作系统
![](./md_image/cloud9/cloud9_4.png)

1. 信息汇总页面点击“Create environment”按钮确认创建Cloud9
![](./md_image/cloud9/cloud9_5.png)

1. 等待Cloud9创建完毕
![](./md_image/cloud9/cloud9_6.png)

1. 创建完毕后进入Cloud9工作界面
![](./md_image/cloud9/cloud9_7.png)

1. 最后在Cloud9命令行终端执行如下的命令安装AWSIoTPythonSDK：

```shell
sudo pip install AWSIoTPythonSDK
```
