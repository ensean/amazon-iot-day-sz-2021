## Lab2. 通过IoT规则存储数据至DynamoDB

### 实验概述

在这个实验中我们将演示如何将AWS IoT收集到的传感器数据通过Rule Engine保存至DynamoDB中

### 架构图

![](md_image/diagram-lab02.png)

### 1. 创建存储数据的DynamoDB表

1. 进入DynamoDB控制台
![](md_image/rules/2021-04-28-11-54-27.png)

2. 点击“创建表”开始创建DynamoDB表
![](md_image/rules/2021-04-28-11-55-48.png)

3. 填入表名（如car_sensor_data)。在主键设置中，设置项目键为“car_name”(类型为字符串)，设置排序键为“timestamp”(类型为数字)。其余保持默认配置，点击创建，等待表创建完成。
![](md_image/rules/2021-05-08-23-54-05.png)

### 2. 创建IoT规则

1. 进入IoT core控制台，菜单栏选择“规则”后在控制面板点击“创建规则”按钮
![](md_image/rules/2021-05-08-23-58-13.png)

2. 输入规则名称，如store_data_to_ddbv2
![](md_image/rules/2021-05-08-23-59-52.png)

3. 规则查询语句部分输入如下内容。
```
SELECT 
  car_name, timestamp, temperature, speed, rpm, location, battery
FROM 
  '#'
```
![](md_image/rules/2021-05-09-00-07-21.png)

4. 点击“添加操作”按钮，启动操作配置向导
![](md_image/rules/2021-05-09-00-08-37.png)

5. 动作类型选择“将消息拆分到DyanmoDB表的多个列（DynamoDBv2）”
![](md_image/rules/2021-05-09-00-09-50.png)

6. 表名选择之前创建的DynamoDB表名，如car_sensor_data。点击“创建角色”按钮，为操作配置角色
![](md_image/rules/2021-05-09-00-11-12.png)

7. 输入角色名，如role_iot_car_to_ddbv2
![](md_image/rules/2021-05-09-00-12-00.png)

8. 角色配置完成后点击“添加操作”按钮
![](md_image/rules/2021-05-09-00-12-25.png)

9. 点击“创建规则”安全，完成规则创建
![](md_image/rules/2021-05-09-00-12-49.png)

10. 确认规则创建成功
![](md_image/rules/2021-05-09-00-13-17.png)

11. 确保Lab1中Python脚本仍然在模拟IoT设备推送数据后，查看DynamoDB表确认数据已保存
![](md_image/rules/2021-05-09-00-14-56.png)

恭喜，至此您已经实现通过IoT规则将设备上报数据保存至DynamoDB。
