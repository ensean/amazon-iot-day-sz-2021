# Amazon华南IoT Day 2021动手实验指引

## 内容框架
### Amazon IoT 基本功能

#### [Lab0:模拟设备环境准备](docs/lab/iot-connect/00_cloud9.md)
在这个实验中我们将会演示如何启动Cloud9 Web IDE环境，后续实验会以此实例运行相应的python脚本，以模拟IoT设备行为。

#### [Lab1:创建设备并发送数据到IoT](docs/lab/iot-connect/01_connect_publish.md)
在这个实验中我们将演示如何创建一个IoT Thing并将模拟设备接入Amazon IoT，通过模拟设备向Amazon IoT云端发送传感器数据

#### [Lab2:通过规则引擎将IoT设备采集数据保存至DynamoDB](docs/lab/iot-connect/02_rules_engine_to_ddb.md)
在这个实验中我们将演示如何将IoT设备采集到的数据库通过规则引擎保存至DynamoDB数据库。

#### [Lab3:通过设备影子控制设备状态](docs/lab/iot-connect/03_control.md)
在这个实验中我们将演示如何利用Amazon IoT Shadow来进行设备状态的控制

## Credits
[Randy Lin, Amazon IoT Day 2020](https://github.com/linjungz/awsiotday/)

